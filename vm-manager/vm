#!/usr/bin/env bash
# ---------------------------------------------------------------------
# Script   : vm
# Descrição: Início e desligamento de VM's
# Versão   : 0.0.0
# Data     : 25/02/2021
# Licença  : GNU/GPL v3.0
# ---------------------------------------------------------------------
# Copyright (C) 2020  Blau Araujo <blau@debxp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------
# Uso: vm [NOME] [start|stop]
# ---------------------------------------------------------------------

usage='
Script de início e desligamento de máquinas virtuais.

Erro: número incorreto de parâmetros!

Uso: vm [NOME start|stop]
Sem argumentos, exibe a lista das máquinas.
'

[[ $UID -eq 0 ]] || sudo_cmd='sudo'

run_args() {

    case $2 in
        'start') cmd=start
        ;;
        'stop') cmd=shutdown
        ;;
        *) printf "$usage\n"
           exit
        ;;
    esac

    $sudo_cmd virsh $cmd $1 || exit

    if [[ $cmd == 'start' ]]; then

        echo "Aguardando conexão de rede..."
        until [[ $vm_ip ]]; do
            vm_ip=$(sudo virsh net-dhcp-leases default | awk '$0~"ipv4"{ gsub(/\/.*$/, "", $5); print $5 }')
        done

        read -n1 -p "Quer iniciar uma sessão SSH com $vm_ip ($1) agora (S/n)? " opt
        [[ ${opt,,} == 'n' ]] && exit

        read -p 'Usuário: ' usrname

        [[ $usrname ]] && ssh $usrname@$vm_ip

    fi
}

case $# in
    0) $sudo_cmd virsh list --all
    ;;
    2) run_args "$@"
    ;;
    *) printf "$usage\n"
       exit
    ;;
esac

exit
